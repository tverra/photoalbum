﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace PhotoAlbum
{
	public partial class AlbumOverview : Form
	{
		public static string AlbumDirPath;
		public static bool HasNextPage;
		public static bool HasPreviousPage;

		public AlbumOverview()
		{
			InitializeAlbumSourceFolder();
			InitializeComponent();
			InitializeAlbums();
		}

		private static void InitializeAlbumSourceFolder()
		{
			var picturesPath = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
			AlbumDirPath = Path.Combine(picturesPath, "Albums");
			Directory.CreateDirectory(AlbumDirPath);
		}

		private void InitializeAlbums()
		{
			var albums = Directory.GetDirectories(AlbumDirPath);

			foreach (var albumPath in albums)
			{
				AddAlbum(albumPath);
			}
		}

		private void AddAlbum(string albumPath)
		{
			var albumBox = new GroupBox();
			var albumImage = new PictureBox();
			var albumName = albumPath.Split(Path.DirectorySeparatorChar).Last();
			var albumCoverPath = albumPath + "\\cover.jpg";

			// 
			// AlbumBox
			// 
			albumBox.Controls.Add(albumImage);
			albumBox.Font = new Font("Microsoft Sans Serif", 12F);
			albumBox.Name = "AlbumBox";
			albumBox.Size = new Size(200, 200);
			albumBox.TabIndex = 0;
			albumBox.TabStop = false;
			albumBox.Text = albumName;
			// 
			// AlbumImage
			// 
			albumImage.Location = new Point(6, 19);
			albumImage.Name = "AlbumImage";
			albumImage.Size = new Size(188, 175);
			albumImage.SizeMode = PictureBoxSizeMode.Zoom;
			albumImage.TabIndex = 0;
			albumImage.TabStop = false;
			albumImage.MouseEnter += (o, e) => { albumBox.BackColor = Color.LightBlue; };
			albumImage.MouseLeave += (o, e) => { albumBox.BackColor = Color.White; };
			if (File.Exists(albumCoverPath))
			{
				albumImage.Image = Image.FromFile(albumCoverPath);
			}
			else
			{
				var genericAlbumCoverPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
					@"res\images\scribble-photo-album-icon.png");
				albumImage.Image = Image.FromFile(genericAlbumCoverPath);
			}
			albumImage.Click += (o, e) =>
			{
				AlbumPage = new AlbumPage(albumPath, 0);
				RootPanel.Controls.Remove(AlbumsView);
				RootPanel.Controls.Add(AlbumPage, 1, 0);
				BackButton.Visible = true;
				NextPageButton.Visible = true;
				PreviousPageButton.Visible = true;
				UpdateControls();
			};

			AlbumsView.Controls.Add(albumBox);
		}

		private void BackButton_Click(object sender, EventArgs e)
		{
			if (AlbumPage == null) return;

			RootPanel.Controls.Remove(AlbumPage);
			RootPanel.Controls.Add(AlbumsView, 1, 0);
			BackButton.Visible = false;
			NextPageButton.Visible = false;
			PreviousPageButton.Visible = false;
		}

		private void NextPageButton_Click(object sender, EventArgs e)
		{
			if (AlbumPage == null) return;

			var albumPath = AlbumPage.AlbumPath;
			var page = AlbumPage.Page;
			RootPanel.Controls.Remove(AlbumPage);
			AlbumPage = new AlbumPage(albumPath, page + 1);
			RootPanel.Controls.Add(AlbumPage, 1, 0);

			UpdateControls();
		}

		private void PreviousPageButton_Click(object sender, EventArgs e)
		{
			if (AlbumPage == null) return;

			var albumPath = AlbumPage.AlbumPath;
			var page = AlbumPage.Page;
			RootPanel.Controls.Remove(AlbumPage);
			AlbumPage = new AlbumPage(albumPath, page - 1);
			RootPanel.Controls.Add(AlbumPage, 1, 0);

			UpdateControls();
		}

		private void UpdateControls()
		{
			NextPageButton.Enabled = HasNextPage;
			PreviousPageButton.Enabled = HasPreviousPage;
		}

		private void BackButton_MouseEnter(object sender, EventArgs e)
		{
			SetButtonHoverEffect(BackButton);
		}

		private void PreviousPageButton_MouseEnter(object sender, EventArgs e)
		{
			SetButtonHoverEffect(PreviousPageButton);
		}

		private void NextPageButton_MouseEnter(object sender, EventArgs e)
		{
			SetButtonHoverEffect(NextPageButton);
		}

		private void BackButton_MouseLeave(object sender, EventArgs e)
		{
			RemoveButtonHoverEffect(BackButton);
		}

		private void PreviousPageButton_MouseLeave(object sender, EventArgs e)
		{
			RemoveButtonHoverEffect(PreviousPageButton);
		}

		private void NextPageButton_MouseLeave(object sender, EventArgs e)
		{
			RemoveButtonHoverEffect(NextPageButton);
		}

		private static void SetButtonHoverEffect(Control button)
		{
			button.BackColor = Color.DarkGray;
		}

		private static void RemoveButtonHoverEffect(Control button)
		{
			button.BackColor = Color.DimGray;
		}
	}
}
