﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media.Imaging;
using Microsoft.WindowsAPICodePack.Shell;
using Microsoft.WindowsAPICodePack.Shell.PropertySystem;

namespace PhotoAlbum
{
	public sealed partial class AlbumPage : UserControl
	{
		public string AlbumPath { get; set; }
		public int Page { get; set; }
		private List<string> _imagePathList;
		private const int NumberOfPicturesOnPage = 4;

		public AlbumPage(string albumPath, int page)
		{
			AlbumPath = albumPath;
			Page = page;

			Dock = DockStyle.Fill;

			InitializeComponent();
			UpdateControls();

			new Thread(() =>
				{
					Thread.CurrentThread.IsBackground = true;
					SetContent();
				}
			).Start();
		}

		private void UpdateControls()
		{
			_imagePathList = Directory.GetFiles(AlbumPath).ToList();
			AlbumOverview.HasNextPage = Page * NumberOfPicturesOnPage + (NumberOfPicturesOnPage) < _imagePathList.Count;
			AlbumOverview.HasPreviousPage = Page > 0;
		}

		private async void SetContent()
		{
			if (_imagePathList.Count < 1) return;
			_imagePathList.Remove(Path.Combine(AlbumPath, "cover.jpg"));

			for (var i = 0; i < NumberOfPicturesOnPage; i++)
			{
				SetPicture(i);
			}
		}

		private void SetPicture(int position)
		{
			var pictureAndDesc = new TableLayoutPanel();
			var picture = new PictureBox();
			var pictureDesc = new TextBox();

			// 
			// pictureAndDesc
			// 
			pictureAndDesc.Anchor = ((((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Left) | AnchorStyles.Right));
			pictureAndDesc.ColumnCount = 1;
			pictureAndDesc.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
			pictureAndDesc.Controls.Add(picture, 0, 0);
			pictureAndDesc.Location = new Point(6, 6);
			pictureAndDesc.Margin = new Padding(6);
			pictureAndDesc.Name = "pictureAndDesc";
			pictureAndDesc.RowCount = 2;
			pictureAndDesc.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
			pictureAndDesc.RowStyles.Add(new RowStyle(SizeType.Absolute, 54F));
			pictureAndDesc.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
			pictureAndDesc.Size = new Size(327, 200);
			pictureAndDesc.TabIndex = 0;
			// 
			// picture
			// 
			picture.Anchor = ((((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Left) | AnchorStyles.Right));
			picture.Location = new Point(3, 3);
			picture.Name = "picture";
			picture.Size = new Size(321, 140);
			picture.SizeMode = PictureBoxSizeMode.Zoom;
			picture.TabIndex = 0;
			picture.TabStop = false;
			// 
			// pictureDesc
			// 
			pictureDesc.Anchor = ((((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Left) | AnchorStyles.Right));
			pictureDesc.Font = new Font("Microsoft Sans Serif", 12F, FontStyle.Regular, GraphicsUnit.Point, 0);
			pictureDesc.Location = new Point(3, 149);
			pictureDesc.Multiline = true;
			pictureDesc.Name = "pictureDesc";
			pictureDesc.Size = new Size(321, 48);
			pictureDesc.TabIndex = 1;

			if ((Page * NumberOfPicturesOnPage) + position < _imagePathList.Count && Page >= 0)
			{
				var imagePath = _imagePathList[(Page * NumberOfPicturesOnPage) + position];
				var imageFile = ShellFile.FromFilePath(imagePath);
				pictureDesc.Text = imageFile.Properties.System.Comment.Value;

				picture.Image = LoadImageToMemory(imagePath);
				pictureAndDesc.Controls.Add(pictureDesc, 0, 1);

				pictureDesc.KeyUp += (o, e) =>
				{
					try
					{
						imageFile.Properties.System.Comment.Value = pictureDesc.Text.Trim();
					}
					catch (PropertySystemException)
					{
						// Notify the user
					}
				};
			}

			switch (position)
			{
				case 0:
					RootPanel.Invoke((MethodInvoker) delegate { RootPanel.Controls.Add(pictureAndDesc, 0, 0); });
					break;
				case 1:
					RootPanel.Invoke((MethodInvoker) delegate { RootPanel.Controls.Add(pictureAndDesc, 1, 0); });
					break;
				case 2:
					RootPanel.Invoke((MethodInvoker) delegate { RootPanel.Controls.Add(pictureAndDesc, 0, 1); });
					break;
				case 3:
					RootPanel.Invoke((MethodInvoker) delegate { RootPanel.Controls.Add(pictureAndDesc, 1, 1); });
					break;
				default:
					throw new IndexOutOfRangeException("The layout is not designed to handle this");
			}
		}

		private static Bitmap LoadImageToMemory(string imagePath)
		{
			var resolution = Screen.PrimaryScreen.Bounds;

			var bitmapImage = new BitmapImage();
			bitmapImage.BeginInit();
			bitmapImage.UriSource = new Uri(imagePath);
			bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
			bitmapImage.DecodePixelWidth = resolution.Width / 4;
			bitmapImage.EndInit();

			using (var outStream = new MemoryStream())
			{
				BitmapEncoder enc = new BmpBitmapEncoder();
				enc.Frames.Add(BitmapFrame.Create(bitmapImage));
				enc.Save(outStream);
				var bitmap = new Bitmap(outStream);

				return new Bitmap(bitmap);
			}
		}
	}
}
