﻿namespace PhotoAlbum
{
	partial class AlbumOverview
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AlbumOverview));
			this.BackButton = new System.Windows.Forms.PictureBox();
			this.PreviousPageButton = new System.Windows.Forms.PictureBox();
			this.AlbumsView = new System.Windows.Forms.FlowLayoutPanel();
			this.NextPageButton = new System.Windows.Forms.PictureBox();
			this.RootPanel = new System.Windows.Forms.TableLayoutPanel();
			this.MenuPanel = new System.Windows.Forms.FlowLayoutPanel();
			((System.ComponentModel.ISupportInitialize)(this.BackButton)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.PreviousPageButton)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.NextPageButton)).BeginInit();
			this.RootPanel.SuspendLayout();
			this.MenuPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// BackButton
			// 
			this.BackButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.BackButton.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.BackButton.Image = ((System.Drawing.Image)(resources.GetObject("BackButton.Image")));
			this.BackButton.Location = new System.Drawing.Point(3, 6);
			this.BackButton.Name = "BackButton";
			this.BackButton.Size = new System.Drawing.Size(67, 50);
			this.BackButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.BackButton.TabIndex = 0;
			this.BackButton.TabStop = false;
			this.BackButton.Visible = false;
			this.BackButton.Click += new System.EventHandler(this.BackButton_Click);
			this.BackButton.MouseEnter += new System.EventHandler(this.BackButton_MouseEnter);
			this.BackButton.MouseLeave += new System.EventHandler(this.BackButton_MouseLeave);
			// 
			// PreviousPageButton
			// 
			this.PreviousPageButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.PreviousPageButton.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.PreviousPageButton.Image = ((System.Drawing.Image)(resources.GetObject("PreviousPageButton.Image")));
			this.PreviousPageButton.Location = new System.Drawing.Point(3, 118);
			this.PreviousPageButton.Name = "PreviousPageButton";
			this.PreviousPageButton.Size = new System.Drawing.Size(67, 50);
			this.PreviousPageButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.PreviousPageButton.TabIndex = 2;
			this.PreviousPageButton.TabStop = false;
			this.PreviousPageButton.Visible = false;
			this.PreviousPageButton.Click += new System.EventHandler(this.PreviousPageButton_Click);
			this.PreviousPageButton.MouseEnter += new System.EventHandler(this.PreviousPageButton_MouseEnter);
			this.PreviousPageButton.MouseLeave += new System.EventHandler(this.PreviousPageButton_MouseLeave);
			// 
			// AlbumsView
			// 
			this.AlbumsView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.AlbumsView.AutoScroll = true;
			this.AlbumsView.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.AlbumsView.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.AlbumsView.Location = new System.Drawing.Point(73, 0);
			this.AlbumsView.Margin = new System.Windows.Forms.Padding(0);
			this.AlbumsView.Name = "AlbumsView";
			this.AlbumsView.Padding = new System.Windows.Forms.Padding(10);
			this.AlbumsView.Size = new System.Drawing.Size(727, 450);
			this.AlbumsView.TabIndex = 0;
			// 
			// NextPageButton
			// 
			this.NextPageButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.NextPageButton.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.NextPageButton.Image = ((System.Drawing.Image)(resources.GetObject("NextPageButton.Image")));
			this.NextPageButton.Location = new System.Drawing.Point(3, 62);
			this.NextPageButton.Name = "NextPageButton";
			this.NextPageButton.Size = new System.Drawing.Size(67, 50);
			this.NextPageButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.NextPageButton.TabIndex = 1;
			this.NextPageButton.TabStop = false;
			this.NextPageButton.Visible = false;
			this.NextPageButton.Click += new System.EventHandler(this.NextPageButton_Click);
			this.NextPageButton.MouseEnter += new System.EventHandler(this.NextPageButton_MouseEnter);
			this.NextPageButton.MouseLeave += new System.EventHandler(this.NextPageButton_MouseLeave);
			// 
			// RootPanel
			// 
			this.RootPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.RootPanel.AutoSize = true;
			this.RootPanel.BackColor = System.Drawing.Color.White;
			this.RootPanel.ColumnCount = 2;
			this.RootPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 73F));
			this.RootPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.RootPanel.Controls.Add(this.MenuPanel, 0, 0);
			this.RootPanel.Controls.Add(this.AlbumsView, 1, 0);
			this.RootPanel.Location = new System.Drawing.Point(0, 0);
			this.RootPanel.Name = "RootPanel";
			this.RootPanel.RowCount = 1;
			this.RootPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.RootPanel.Size = new System.Drawing.Size(800, 450);
			this.RootPanel.TabIndex = 3;
			// 
			// MenuPanel
			// 
			this.MenuPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.MenuPanel.BackColor = System.Drawing.Color.DimGray;
			this.MenuPanel.Controls.Add(this.BackButton);
			this.MenuPanel.Controls.Add(this.NextPageButton);
			this.MenuPanel.Controls.Add(this.PreviousPageButton);
			this.MenuPanel.Location = new System.Drawing.Point(0, 0);
			this.MenuPanel.Margin = new System.Windows.Forms.Padding(0);
			this.MenuPanel.Name = "MenuPanel";
			this.MenuPanel.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
			this.MenuPanel.Size = new System.Drawing.Size(73, 450);
			this.MenuPanel.TabIndex = 0;
			// 
			// AlbumOverview
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.ClientSize = new System.Drawing.Size(800, 450);
			this.Controls.Add(this.RootPanel);
			this.Name = "AlbumOverview";
			this.Text = "Photo Albums";
			((System.ComponentModel.ISupportInitialize)(this.BackButton)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.PreviousPageButton)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.NextPageButton)).EndInit();
			this.RootPanel.ResumeLayout(false);
			this.MenuPanel.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.FlowLayoutPanel AlbumsView;
		private System.Windows.Forms.PictureBox BackButton;
		private AlbumPage AlbumPage;
		private System.Windows.Forms.PictureBox PreviousPageButton;
		private System.Windows.Forms.PictureBox NextPageButton;
		private System.Windows.Forms.TableLayoutPanel RootPanel;
		private System.Windows.Forms.FlowLayoutPanel MenuPanel;
	}
}

